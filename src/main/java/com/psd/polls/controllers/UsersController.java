package com.psd.polls.controllers;

import java.util.List;

import javax.validation.Valid;

import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.dto.BasicEntity;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.entities.dto.UserProfile;
import com.psd.polls.entities.dto.UsersListParams;
import com.psd.polls.entities.dto.ValueExists;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.services.UsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping(path = "")
    public OutputList<User> listUsers(@Valid UsersListParams usersListParams) {
        return usersService.getUsersList(usersListParams);
    }

    @GetMapping(path = "filter")
    public List<BasicEntity> filterUsers(@RequestParam(name = "name", defaultValue = "") String name) {
        return usersService.getUsersFilter(name);
    }

    @GetMapping(path = "unique")
    public ValueExists checkIfUsernameExists(@RequestParam(name = "username", defaultValue = "") String username) {
        return new ValueExists(usersService.usernameExists(username));
    }

    @PostMapping(path = "")
    public User createUser(@Validated(NewGroup.class) @RequestBody UserProfile usersProfile) {
        return usersService.createUser(usersProfile);
    }

    @GetMapping(path = "{userId}")
    public User getUser(@PathVariable(required = true, name = "userId") @Valid User user) {
        return user;
    }

    @PutMapping(path = "{userId}")
    public User editUser(
        @PathVariable(required = true, name = "userId") @Valid User user,
        @Validated(EditGroup.class) @RequestBody UserProfile usersProfile
    ) {
        return usersService.editUser(user, usersProfile);
    }

    @PatchMapping(path = "{userId}/toggle")
    public void toggleUser(@PathVariable(required = true, name = "userId") @Valid User user) {
        usersService.toggleUser(user);
    }

    @DeleteMapping(path = "{userId}")
    public void deleteUser(@PathVariable(required = true, name = "userId") @Valid User user) {
        usersService.deleteUser(user);
    }
}
