package com.psd.polls.controllers;

import com.psd.polls.entities.dao.Party;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.services.PartiesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/parties")
public class PartiesController {

    @Autowired
    private PartiesService partiesService;

    @GetMapping("")
    public OutputList<Party> listPolls(@RequestParam(name = "name", defaultValue = "") String name) {
        return partiesService.getPartiesList(name);
    }
}
