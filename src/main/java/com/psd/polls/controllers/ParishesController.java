package com.psd.polls.controllers;

import java.util.List;

import com.psd.polls.entities.dao.Parish;
import com.psd.polls.services.ParishesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/parishes")
public class ParishesController {

    @Autowired
    private ParishesService parishesService;

    @GetMapping(path = "")
    public List<Parish> listParishes(@RequestParam(name = "parish", defaultValue = "") String name) {
        return parishesService.getParishes(name);
    }

    @GetMapping(path = "users/filter")
    public List<Parish> filterParihes(@RequestParam(name = "parish", defaultValue = "") String name) {
        return parishesService.getParishesFromUsers(name);
    }
}
