package com.psd.polls.controllers;

import javax.servlet.http.HttpServletResponse;

import com.psd.polls.services.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping(path = "logout")
    public void listUsers(HttpServletResponse response) {
        authService.doLogout(response);
    }
}
