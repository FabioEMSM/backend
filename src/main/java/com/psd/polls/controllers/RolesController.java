package com.psd.polls.controllers;

import com.psd.polls.entities.dao.Role;
import com.psd.polls.services.RolesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/roles")
public class RolesController {

    @Autowired
    private RolesService rolesService;

    @GetMapping("")
    public Iterable<Role> getRoles() {
        return rolesService.getRoles();
    }

}
