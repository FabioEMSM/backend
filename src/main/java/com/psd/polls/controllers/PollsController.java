package com.psd.polls.controllers;

import java.util.List;

import javax.validation.Valid;

import com.psd.polls.entities.dao.Poll;
import com.psd.polls.entities.dto.BasicEntity;
import com.psd.polls.entities.dto.BasicPoll;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.entities.dto.OwnTablesPoll;
import com.psd.polls.entities.dto.PollData;
import com.psd.polls.entities.dto.PollsListParams;
import com.psd.polls.entities.dto.ValueExists;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.services.PollsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/polls")
public class PollsController {

    @Autowired
    private PollsService pollsService;

    @GetMapping("")
    public OutputList<BasicPoll> listPolls(@Valid PollsListParams pollsListParams) {
        return pollsService.getPollsList(pollsListParams);
    }

    @GetMapping("{pollId}")
    public PollData getPoll(@PathVariable(required = true, name = "pollId") @Valid Poll poll) {
        return pollsService.getPollData(poll);
    }

    @PostMapping("")
    public BasicEntity createPoll(@Validated(NewGroup.class) @RequestBody PollData pollData) {
        Poll newPoll = pollsService.createPoll(pollData);
        return new BasicEntity(newPoll.getId(), newPoll.getName());
    }

    @PutMapping("{pollId}")
    public void editPoll(
        @Validated(EditGroup.class) @RequestBody PollData pollData,
        @PathVariable(required = true, name = "pollId") @Valid Poll poll
    ) {
        pollsService.editPoll(pollData, poll);
    }

    @DeleteMapping("{pollId}")
    public void deletePoll(@PathVariable(required = true, name = "pollId") @Valid Poll poll) {
        pollsService.deletePoll(poll);
    }

    @PatchMapping("{pollId}/lock-edition")
    public void lockPollEdition(@PathVariable(required = true, name = "pollId") @Valid Poll poll) {
        pollsService.lockPollEdition(poll);
    }

    @PatchMapping("{pollId}/finalize")
    public void finalizePoll(@PathVariable(required = true, name = "pollId") @Valid Poll poll) {
        pollsService.finalizePoll(poll);
    }

    @GetMapping(path = "filter")
    public List<BasicEntity> filterPolls(@RequestParam(name = "name", defaultValue = "") String name) {
        return pollsService.getPollsFilter(name);
    }

    @GetMapping(path = "owned-tables")
    public List<OwnTablesPoll> ownTables() {
        return pollsService.getOwnTables();
    }

    @GetMapping(path = "unique")
    public ValueExists checkIfUsernameExists(
        @RequestParam(name = "poll", defaultValue = "") String poll,
        @RequestParam(name = "id", defaultValue = "") Long pollId
    ) {
        return new ValueExists(pollsService.pollNameExists(poll, pollId));
    }

}
