package com.psd.polls.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = ExistingIdValidator.class)
@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExistingId {
    String idField() default "id";
    Class<?> entity();
    String message() default "This item does not exist!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
