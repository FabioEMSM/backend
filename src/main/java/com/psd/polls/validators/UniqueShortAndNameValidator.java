package com.psd.polls.validators;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psd.polls.entities.dto.polldata.ShortAndNameData;

public class UniqueShortAndNameValidator
implements ConstraintValidator<UniqueShortAndName, Collection<? extends ShortAndNameData>> {

    @Override
    public void initialize(UniqueShortAndName uniqueShortAndName) {
        // Nothing to do here
    }

    @Override
    public boolean isValid(Collection<? extends ShortAndNameData> collection, ConstraintValidatorContext context) {
        if (collection.isEmpty()) {
            return true;
        }

        return !(
            hasRepeatedNames(extractShortNames(collection)) ||
            hasRepeatedNames(extractNames(collection))
        );
    }

    private boolean hasRepeatedNames (String[] names) {
        for (int i = 0; i < names.length - 1; ++i) {
            for (int j = i + 1; j < names.length; ++j) {
                if (names[i].equalsIgnoreCase(names[j])) {
                    return true;
                }
            }
        }

        return false;
    }

    private String[] extractShortNames(Collection<? extends ShortAndNameData> collection) {
        return collection.stream()
            .filter(item -> item.getShortName() != null && !item.getShortName().trim().isEmpty())
            .map(ShortAndNameData::getShortName)
            .toArray(String[]::new);
    }

    private String[] extractNames(Collection<? extends ShortAndNameData> collection) {
        return collection.stream()
            .filter(item -> item.getName() != null && !item.getName().trim().isEmpty())
            .map(ShortAndNameData::getName)
            .toArray(String[]::new);
    }

}