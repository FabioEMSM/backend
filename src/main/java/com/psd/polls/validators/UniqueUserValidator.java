package com.psd.polls.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psd.polls.services.UsersService;

import org.springframework.beans.factory.annotation.Autowired;

public class UniqueUserValidator implements ConstraintValidator<UniqueUser, String> {

    @Autowired
    private UsersService usersService;

    @Override
    public void initialize(UniqueUser uniqueUser) {
        // Nothing to do here
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        return username == null || username.trim().equals("") ||
            !usersService.usernameExists(username);
    }

}