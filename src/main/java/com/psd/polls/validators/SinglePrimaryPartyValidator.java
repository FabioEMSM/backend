package com.psd.polls.validators;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psd.polls.entities.dto.polldata.PartyData;

public class SinglePrimaryPartyValidator implements ConstraintValidator<SinglePrimaryParty, Collection<PartyData>> {

    @Override
    public void initialize(SinglePrimaryParty singlePrimaryParty) {
        // Nothing to do here
    }

    @Override
    public boolean isValid(Collection<PartyData> parties, ConstraintValidatorContext context) {
        if (parties == null || parties.isEmpty()) {
            return true;
        }

        Long numberOfPrimaryParties = parties.stream()
            .filter(PartyData::getPrimary)
            .count();

        return numberOfPrimaryParties == 1;
    }

}