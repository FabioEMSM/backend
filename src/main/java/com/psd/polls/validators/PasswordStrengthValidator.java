package com.psd.polls.validators;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordStrengthValidator implements ConstraintValidator<PasswordStrength, String> {

    private float min;

    @Override
    public void initialize(PasswordStrength passwordStrength) {
        this.min = passwordStrength.min();
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        return password == null || password.equals("") || min <= calcStrength(password);
    }

    private Float calcStrength(final String password) {
        if (password == null || password.length() < 1) {
            return 0f;
        }

        // Count repeating characters
        HashMap<Character, Float> letters = new HashMap<>();
        for (final Character character : password.toCharArray()) {
            Float value = letters.get(character);
            if (value == null) {
                letters.put(character, 1f);
            }
            else {
                letters.replace(character, value + 1);
            }
        }

        Float score = 0f;
        for(final Float value : letters.values()) {
            score += 5.0f / Math.min(value, 5);
        }

        // The bigger the entropy, the bigger the score
        score +=
            countRegExOccurences("\\d+/", password) * 2.5f +
            (
                countRegExOccurences("[a-z]+", password) +
                countRegExOccurences("[A-Z]+", password)
            ) * 1.5f +
            countRegExOccurences("\\w+", password) * 4f +
            countRegExOccurences("\\W", password) * 5f;

        return Math.min(score, 100);
    }

    private int countRegExOccurences(String regex, String text) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        int matches = 0;
        while(matcher.find()) {
            ++matches;
        }

        return matches;
    }
}
