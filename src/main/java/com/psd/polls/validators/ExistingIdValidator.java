package com.psd.polls.validators;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.web.context.WebApplicationContext;

public class ExistingIdValidator implements ConstraintValidator<ExistingId, Object> {

    private String idFieldName;

    @Autowired
    private WebApplicationContext appContext;

    private CrudRepository<?, Long> repository;

    @Override
    public void initialize(ExistingId existingId) {
        idFieldName = existingId.idField();
        Class<?> entityType = existingId.entity();

        Optional<?> repositoryOptional = (new Repositories(appContext)).getRepositoryFor(entityType);

        if (repositoryOptional.isPresent()) {
            repository = (CrudRepository<?, Long>)repositoryOptional.get();
        }
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        if (object == null) {
            return true;
        }

        Long id = (Long)(new BeanWrapperImpl(object)).getPropertyValue(idFieldName);

        if (id == null) {
            return true;
        }

        return repository.existsById(id);
    }

}
