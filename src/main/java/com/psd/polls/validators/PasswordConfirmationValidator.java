package com.psd.polls.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class PasswordConfirmationValidator implements ConstraintValidator<PasswordConfirmation, Object> {

    private String passwordFieldName;

    private String confirmationFieldName;

    @Override
    public void initialize(PasswordConfirmation passwordConfirmation) {
        passwordFieldName = passwordConfirmation.passwordField();
        confirmationFieldName = passwordConfirmation.confirmationField();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        if (object == null) {
            return true;
        }

        BeanWrapper objectWrapper = new BeanWrapperImpl(object);
        String password = (String)objectWrapper.getPropertyValue(passwordFieldName);
        String confirmation = (String)objectWrapper.getPropertyValue(confirmationFieldName);

        return (password == null && confirmation == null) ||
            (password != null && confirmation != null && password.equals(confirmation));
    }

}
