package com.psd.polls.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PasswordConfirmationValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordConfirmation {
    String passwordField() default "password";
    String confirmationField() default "passwordConfirmation";
    String message() default "Password and Confirmation are not equal!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
