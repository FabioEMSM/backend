package com.psd.polls.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psd.polls.entities.dto.polldata.PollGeneralData;
import com.psd.polls.services.PollsService;

import org.springframework.beans.factory.annotation.Autowired;

public class UniquePollNameValidator implements ConstraintValidator<UniquePollName, PollGeneralData> {

    @Autowired
    private PollsService pollsService;

    @Override
    public void initialize(UniquePollName uniquePollName) {
        // Nothing to do here
    }

    @Override
    public boolean isValid(PollGeneralData pollGeneralData, ConstraintValidatorContext context) {
        return pollGeneralData.getName() == null ||
            !pollsService.pollNameExists(pollGeneralData.getName(), pollGeneralData.getId());
    }

}