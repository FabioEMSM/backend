package com.psd.polls.configurations.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class GenerateCookieString {

    private GenerateCookieString() {}

    public static final String getCookieHeaderString(
        String cookieName,
        String cookieValue,
        Integer maxAge,
        Date expiration
    ) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        return cookieName + "=" + cookieValue + "; Max-Age=" + maxAge +
                "; Expires=" + simpleDateFormat.format(expiration) +
                "; Domain=localhost; Path=/; SameSite=none; Secure; HttpOnly;";
    }
}
