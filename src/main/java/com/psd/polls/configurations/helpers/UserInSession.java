package com.psd.polls.configurations.helpers;

import java.util.ArrayList;

import com.psd.polls.entities.dao.User;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public final class UserInSession {

    private UserInSession() {}

    public static final User get() {
        Object userSession = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(userSession instanceof User) {
            return (User) userSession;
        }

        return null;
    }

    public static final void destroy() {
        UsernamePasswordAuthenticationToken uPAT = new UsernamePasswordAuthenticationToken(
            null,
            null,
            new ArrayList<>()
        );
        uPAT.setAuthenticated(false);

        SecurityContextHolder.getContext().setAuthentication(uPAT);
    }
}
