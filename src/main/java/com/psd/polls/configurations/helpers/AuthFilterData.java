package com.psd.polls.configurations.helpers;

import com.psd.polls.services.UsersService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthFilterData {
    private String tokenAuthSecret;

    private Long tokenAuthExpiration;
    
    private String tokenAccessSecret;
    
    private Long tokenAccessExpiration;
    
    private UsersService usersService;
    
    private AuthenticationManager authenticationManager;

    private PasswordEncoder passwordEncoder;

    public String getTokenAuthSecret() {
        return tokenAuthSecret;
    }

    public void setTokenAuthSecret(String tokenAuthSecret) {
        this.tokenAuthSecret = tokenAuthSecret;
    }

    public Long getTokenAuthExpiration() {
        return tokenAuthExpiration;
    }

    public void setTokenAuthExpiration(Long tokenAuthExpiration) {
        this.tokenAuthExpiration = tokenAuthExpiration;
    }

    public String getTokenAccessSecret() {
        return tokenAccessSecret;
    }

    public void setTokenAccessSecret(String tokenAccessSecret) {
        this.tokenAccessSecret = tokenAccessSecret;
    }

    public Long getTokenAccessExpiration() {
        return tokenAccessExpiration;
    }

    public void setTokenAccessExpiration(Long tokenAccessExpiration) {
        this.tokenAccessExpiration = tokenAccessExpiration;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
