package com.psd.polls.configurations;

import java.util.Arrays;

import com.psd.polls.configurations.filters.JWTAuthenticationFilter;
import com.psd.polls.configurations.filters.JWTAuthorizationFilter;
import com.psd.polls.configurations.filters.JWTRefreshAuthenticationFilter;
import com.psd.polls.configurations.helpers.AuthFilterData;
import com.psd.polls.services.UsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Value("${session.auth.secret}")
    private String authTokenSecret;

    @Value("${session.auth.expire}")
    private Long authTokenExpire;

    @Value("${session.access.secret}")
    private String accessTokenSecret;

    @Value("${session.access.expire}")
    private Long accessTokenExpire;

    @Autowired
    private PasswordEncription passwordEncription;

    @Autowired
    private UsersService usersService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        AuthFilterData authFilterData = new AuthFilterData();
        authFilterData.setAuthenticationManager(authenticationManager());
        authFilterData.setUsersService(usersService);
        authFilterData.setPasswordEncoder(passwordEncription.encoder());
        authFilterData.setTokenAuthSecret(authTokenSecret);
        authFilterData.setTokenAuthExpiration(authTokenExpire);
        authFilterData.setTokenAccessSecret(accessTokenSecret);
        authFilterData.setTokenAccessExpiration(accessTokenExpire);

        http.cors().and()
            .csrf().disable()
            .antMatcher("/public-api/**").authorizeRequests()
                .antMatchers("/public-api/**").permitAll().and()
            .antMatcher("/api/auth/login").authorizeRequests()
                .antMatchers("/api/auth/login").anonymous().and()
                .addFilter(new JWTAuthenticationFilter(authFilterData))
            .antMatcher("/api/auth/refresh").authorizeRequests()
                .antMatchers("/api/auth/refresh").anonymous().and()
                .addFilter(new JWTRefreshAuthenticationFilter(authFilterData))
            .antMatcher("/api/**").authorizeRequests()
                .antMatchers("/api/**").authenticated().anyRequest().permitAll().and()
                .addFilter(new JWTAuthorizationFilter(authFilterData))
            // this disables session creation on Spring Security
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedMethods(
            Arrays.asList(
                "DELETE",
                "GET",
                "HEAD",
                "OPTIONS",
                "PATCH",
                "POST",
                "PUT",
                "TRACE"
            )
        );

        corsConfiguration.setMaxAge(3600L);
        corsConfiguration.setAllowedHeaders(
            Arrays.asList(
                "Content-Type",
                "Accept",
                "X-Requested-With",
                "remember-me",
                "Referer",
                "Authorization",
                "Access-Control-Request-Headers",
                "Access-Control-Request-Method"
            )
        );
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOriginPatterns(
            Arrays.asList(
                "*",
                "localhost",
                "localhost:8444",
                "localhost:4200",
                "127.0.0.1"
            )
        );

        corsConfiguration.setExposedHeaders(
            Arrays.asList(
                "Auth-token",
                "Auth-expire",
                "Access-Control-Allow-Origin",
                "Access-Control-Allow-Credentials"
            )
        );

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);

        return source;
    }
}
