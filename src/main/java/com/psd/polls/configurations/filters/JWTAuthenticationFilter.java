package com.psd.polls.configurations.filters;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.psd.polls.configurations.helpers.AuthFilterData;
import com.psd.polls.entities.dto.UserLogin;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.server.ResponseStatusException;

public class JWTAuthenticationFilter extends JWTSuccessAuthFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthFilterData authFilterData) {
        super(authFilterData);
        this.authenticationManager = authFilterData.getAuthenticationManager();
        setFilterProcessesUrl("/api/auth/login"); 
    }

    @Override
    public Authentication attemptAuthentication(
        HttpServletRequest request,
        HttpServletResponse response
    ) throws AuthenticationException {
        try {
            UserLogin credentials = new ObjectMapper()
                .readValue(request.getInputStream(), UserLogin.class);

            return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    credentials.username,
                    credentials.password,
                    new ArrayList<>()
                )
            );
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

}
