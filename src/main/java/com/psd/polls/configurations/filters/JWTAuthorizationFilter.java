package com.psd.polls.configurations.filters;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.psd.polls.configurations.helpers.AuthFilterData;
import com.psd.polls.entities.dao.User;
import com.psd.polls.services.UsersService;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String HEADER_STRING = "Authorization";

    private String tokenAuthSecret = "SECRET_KEY";

    private UsersService usersService;

    public JWTAuthorizationFilter(AuthFilterData authFilterData) {
        super(authFilterData.getAuthenticationManager());
        tokenAuthSecret = authFilterData.getTokenAuthSecret();
        usersService = authFilterData.getUsersService();
    }

    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain chain
    ) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            response.sendError(401, "Unauthorized!");
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        if (authentication == null) {
            response.sendError(401, "Unauthorized!");
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if(token == null) {
            return null;
        }

        // parse the token.
        String username = JWT.require(Algorithm.HMAC512(tokenAuthSecret.getBytes()))
            .build()
            .verify(token.replace(TOKEN_PREFIX, ""))
            .getSubject();
        if(username == null) {
            return null;
        }

        User user = usersService.loadUserByUsername(username);
        if(user == null) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
    }
}
