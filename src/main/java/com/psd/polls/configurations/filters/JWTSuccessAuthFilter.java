package com.psd.polls.configurations.filters;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.psd.polls.configurations.helpers.AuthFilterData;
import com.psd.polls.configurations.helpers.GenerateCookieString;
import com.psd.polls.entities.dao.User;
import com.psd.polls.services.UsersService;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public abstract class JWTSuccessAuthFilter extends UsernamePasswordAuthenticationFilter {

    private String tokenAuthSecret;

    private Long tokenAuthExpire;

    private String tokenAccessSecret;

    private Long tokenAccessExpire;

    private UsersService usersService;

    protected JWTSuccessAuthFilter (AuthFilterData authFilterData) {
        this.tokenAuthSecret = authFilterData.getTokenAuthSecret();
        this.tokenAuthExpire = authFilterData.getTokenAuthExpiration();
        this.tokenAccessSecret = authFilterData.getTokenAccessSecret();
        this.tokenAccessExpire = authFilterData.getTokenAccessExpiration();
        this.usersService = authFilterData.getUsersService();
    }

    @Override
    protected void successfulAuthentication(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain chain,
        Authentication auth
    ) throws IOException {
        final User user = ((User) auth.getPrincipal());

        setCookie(response, user);
        setAuthToken(response, user);

        response.getWriter().flush();
    }

    private void setAuthToken(HttpServletResponse response, User user) {
        final Date expirationDate = new Date(System.currentTimeMillis() + tokenAuthExpire);
        final String token = JWT.create()
            .withSubject(user.getUsername())
            .withExpiresAt(expirationDate)
            .withClaim("name", user.getName())
            .withClaim("roleId", user.getRole().getId())
            .withClaim("roleName", user.getRole().getName())
            .sign(Algorithm.HMAC512(tokenAuthSecret.getBytes()));

        final String headerToken = user.getUsername() + " " + token;

        response.setHeader("Auth-token", headerToken);
        response.setHeader(
            "Auth-expire",
            (new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z")).format(expirationDate)
        );
    }

    private void setCookie(HttpServletResponse response, User user) {
        final Date refreshExpirationDate = new Date(System.currentTimeMillis() + (tokenAccessExpire * 1000));
        final String refreshToken = JWT.create()
            .withSubject(user.getUsername())
            .withExpiresAt(refreshExpirationDate)
            .sign(Algorithm.HMAC512(tokenAccessSecret));

        user.setRemember(refreshToken);
        usersService.getUsersRepository().save(user);

        response.setHeader(
            "Set-cookie",
            GenerateCookieString.getCookieHeaderString(
                "Auth-refresh",
                refreshToken,
                tokenAccessExpire.intValue(),
                refreshExpirationDate
            )
        );
    }

}
