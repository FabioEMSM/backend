package com.psd.polls.configurations.filters;

import java.util.ArrayList;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.psd.polls.configurations.helpers.AuthFilterData;
import com.psd.polls.entities.dao.User;
import com.psd.polls.services.UsersService;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class JWTRefreshAuthenticationFilter extends JWTSuccessAuthFilter {
    private UsersService usersService;

    public JWTRefreshAuthenticationFilter(AuthFilterData authFilterData) {
        super(authFilterData);
        this.usersService = authFilterData.getUsersService();
        setFilterProcessesUrl("/api/auth/refresh");
    }

    @Override
    public Authentication attemptAuthentication(
        HttpServletRequest request,
        HttpServletResponse response
    ) throws AuthenticationException {
        Cookie cookie = findCookie("Auth-refresh", request.getCookies());
        String cookieValue = (cookie != null) ? cookie.getValue() : "empty";
        User user = usersService.findUserByAccessToken(cookieValue);

        if (cookieValue.isEmpty() || user == null) {
            throw new BadCredentialsException("Access expired");
        }

        return new UsernamePasswordAuthenticationToken(
            user,
            null,
            new ArrayList<>()
        );
    }

    private Cookie findCookie(String cookieName, Cookie[] cookies) {
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase(cookieName)) {
                    return cookie;
                }
            }
        }

        return null;
    }
}