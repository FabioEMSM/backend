package com.psd.polls.repositories;

import com.psd.polls.entities.dao.PollParishParty;
import com.psd.polls.entities.dao.PollParishPartyPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollParishPartiesRepository extends JpaRepository<PollParishParty, PollParishPartyPK> {}
