package com.psd.polls.repositories;

import com.psd.polls.entities.dao.PollParishTableUser;
import com.psd.polls.entities.dao.PollParishTableUserPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollParishTableUsersRepository extends JpaRepository<PollParishTableUser, PollParishTableUserPK> {}
