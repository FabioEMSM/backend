package com.psd.polls.repositories;

import java.util.Date;

import com.psd.polls.entities.dao.Poll;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PollsRepository extends JpaRepository<Poll, Long> {

    @Query(
        "SELECT poll FROM Poll AS poll " +
        " WHERE UPPER(poll.name) LIKE CONCAT('%', UPPER(:searchPoll), '%') AND (" +
        "  :searchDateStart IS NULL OR " +
        "  :searchDateStart <= poll.date " +
        " ) AND ( " +
        "  :searchDateEnd IS NULL OR " +
        "  :searchDateEnd >= poll.date " +
        " )"
    )
    Page<Poll> findPollsBySearchNameAndDate(
        String searchPoll,
        Date searchDateStart,
        Date searchDateEnd,
        Pageable page
    );

    @Query(
        "SELECT poll FROM Poll AS poll " +
        " WHERE UPPER(poll.name) LIKE CONCAT('%', UPPER(:searchPoll), '%')"
    )
    Page<Poll> findPollsBySearchString(
        String searchPoll,
        Pageable page
    );

    boolean existsByNameIgnoreCaseAndIdNot(String name, Long pollId);

}
