package com.psd.polls.repositories;

import java.util.List;
import java.util.Optional;

import com.psd.polls.entities.dao.Parish;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ParishesRepository extends JpaRepository<Parish, Long> {

    @Query(
        "SELECT parish FROM Parish AS parish " +
        " WHERE UPPER(parish.name) LIKE CONCAT('%', UPPER(:searchParish), '%') OR " +
        "  UPPER(parish.shortName) LIKE CONCAT('%', UPPER(:searchParish), '%') " +
        " ORDER BY UPPER(parish.name) ASC"
    )
    List<Parish> findAllParishesBySearchString(String searchParish);

    @Query(
        "SELECT DISTINCT parish FROM Parish AS parish " +
        " JOIN User AS user ON user.parish = parish " +
        " WHERE UPPER(parish.name) LIKE CONCAT('%', UPPER(:searchParish), '%') OR " +
        "  UPPER(parish.shortName) LIKE CONCAT('%', UPPER(:searchParish), '%')"
    )
    Page<Parish> findParishesBySearchString(
        String searchParish,
        Pageable page
    );

    Optional<Parish> findByNameIgnoreCaseAndShortNameIgnoreCase(String name, String shortName);
}
