package com.psd.polls.repositories;

import java.util.Optional;

import com.psd.polls.entities.dao.Party;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PartiesRepository extends JpaRepository<Party, Long> {

    @Query(
        "SELECT party FROM Party AS party " +
        " WHERE UPPER(party.name) LIKE CONCAT('%', UPPER(:searchParty), '%') OR " +
        "  UPPER(party.shortName) LIKE CONCAT('%', UPPER(:searchParty), '%') "
    )
    Page<Party> findPartiesBySearchName(
        String searchParty,
        Pageable page
    );

    Optional<Party> findByNameIgnoreCaseAndShortNameIgnoreCase(String name, String shortName);

}
