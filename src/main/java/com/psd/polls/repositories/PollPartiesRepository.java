package com.psd.polls.repositories;

import com.psd.polls.entities.dao.PollParty;
import com.psd.polls.entities.dao.PollPartyPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollPartiesRepository extends JpaRepository<PollParty, PollPartyPK> {
    void deleteByIdPollId(Long pollId);
}
