package com.psd.polls.repositories;

import com.psd.polls.entities.dao.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    User findByUsernameIgnoreCase(String username);
    User findByRemember(String rememberToken);
    Boolean existsByUsernameIgnoreCase(String username);

    @Query(
        "SELECT user FROM User AS user " +
        " WHERE UPPER(user.name) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.username) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.email) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.phone) LIKE CONCAT('%', UPPER(:searchUser), '%')"
    )
    Page<User> findUsersBySearchString(
        String searchUser,
        Pageable page
    );

    @Query(
        "SELECT user FROM User AS user " +
        " LEFT JOIN Parish as parish " +
        "  ON parish = user.parish" +
        " WHERE (" +
        "  UPPER(user.name) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.username) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.email) LIKE CONCAT('%', UPPER(:searchUser), '%') OR " +
        "  UPPER(user.phone) LIKE CONCAT('%', UPPER(:searchUser), '%') " +
        " ) AND ( " +
        "  :searchParish IS NULL OR " +
        "  :searchParish = '' OR " +
        "  UPPER(parish.name) LIKE CONCAT('%', UPPER(:searchParish), '%') OR " +
        "  UPPER(parish.shortName) LIKE CONCAT('%', UPPER(:searchParish), '%') " +
        " )"
    )
    Page<User> findUsersBySearchNameAndParish(
        String searchUser,
        String searchParish,
        Pageable page
    );
}
