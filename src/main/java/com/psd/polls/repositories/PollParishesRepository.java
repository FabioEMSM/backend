package com.psd.polls.repositories;

import com.psd.polls.entities.dao.PollParish;
import com.psd.polls.entities.dao.PollParishPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PollParishesRepository extends JpaRepository<PollParish, PollParishPK> {}
