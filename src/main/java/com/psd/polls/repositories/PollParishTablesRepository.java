package com.psd.polls.repositories;

import java.util.List;

import com.psd.polls.entities.dao.PollParishTable;
import com.psd.polls.entities.dao.PollParishTablePK;
import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.dto.OwnTablesRaw;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PollParishTablesRepository extends JpaRepository<PollParishTable, PollParishTablePK> {
    @Query(
        "SELECT new com.psd.polls.entities.dto.OwnTablesRaw(poll.id, poll.name, parish.id, parish.name, table.id.tableNumber) " +
        " FROM Poll AS poll " +
        " JOIN poll.parishAssemblies AS pollParish" +
        " JOIN pollParish.id.parish AS parish" +
        " JOIN pollParish.tables AS table" +
        " WHERE " +
        "  poll.isEditable = false AND " +
        "  poll.isClosed = false " +
        " ORDER BY poll.id ASC, parish.id ASC, table.id.tableNumber ASC"
    )
    List<OwnTablesRaw> findAllActiveTables();

    @Query(
        "SELECT new com.psd.polls.entities.dto.OwnTablesRaw(poll.id, poll.name, parish.id, parish.name, table.id.tableNumber) " +
        " FROM Poll AS poll " +
        " JOIN poll.parishAssemblies AS pollParish" +
        " JOIN pollParish.id.parish AS parish" +
        " JOIN pollParish.tables AS table" +
        " JOIN table.users AS tableUser ON tableUser.id.user = :user " +
        " WHERE " +
        "  poll.isEditable = false AND " +
        "  poll.isClosed = false " +
        " ORDER BY poll.id ASC, parish.id ASC, table.id.tableNumber ASC"
    )
    List<OwnTablesRaw> findAllActiveTablesByUser(User user);

    // @Query(
    //     "SELECT table " +
    //     " FROM Poll AS poll " +
    //     " JOIN poll.parishAssemblies AS pollParish" +
    //     " JOIN pollParish.id.parish AS parish" +
    //     " JOIN pollParish.tables AS table" +
    //     " WHERE " +
    //     "  poll.isEditable = false AND " +
    //     "  poll.isClosed = false"
    // )
    // List<PollParishTable> findAllActiveTables();

    // @Query(
    //     "SELECT table " +
    //     " FROM Poll AS poll " +
    //     " JOIN poll.parishAssemblies AS pollParish" +
    //     " JOIN pollParish.id.parish AS parish" +
    //     " JOIN pollParish.tables AS table" +
    //     " JOIN table.users AS tableUser ON tableUser.id.user = :user " +
    //     " WHERE " +
    //     "  poll.isEditable = false AND " +
    //     "  poll.isClosed = false "
    // )
    // List<PollParishTable> findAllActiveTablesByUser(User user);
}
