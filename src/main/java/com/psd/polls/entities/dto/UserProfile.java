package com.psd.polls.entities.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.psd.polls.entities.dao.Parish;
import com.psd.polls.entities.dao.Role;
import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.validators.ExistingId;
import com.psd.polls.validators.PasswordConfirmation;
import com.psd.polls.validators.PasswordStrength;
import com.psd.polls.validators.UniqueUser;

import org.hibernate.validator.constraints.Length;

@PasswordConfirmation(groups = { NewGroup.class, EditGroup.class })
@ExistingId(entity = User.class, groups = { NewGroup.class, EditGroup.class })
public class UserProfile {

    @NotNull(groups = EditGroup.class)
    private Long id;

    @NotBlank(groups = NewGroup.class)
    @Pattern(regexp = "^[0-9a-zA-Z\\-\\._]*$", groups = NewGroup.class)
    @Length(min = 3, max = 20, groups = NewGroup.class)
    @UniqueUser(groups = NewGroup.class)
    private String username;

    @NotBlank(groups = NewGroup.class)
    @Length(min = 8, max = 32, groups = { NewGroup.class, EditGroup.class })
    @PasswordStrength(min = 33.33f, groups = { NewGroup.class, EditGroup.class })
    private String password = null;

    @NotBlank(groups = NewGroup.class)
    private String passwordConfirmation = null;

    @NotBlank(groups = { NewGroup.class, EditGroup.class })
    @Length(min = 3, max = 255, groups = { NewGroup.class, EditGroup.class })
    private String name;

    @Valid
    @NotNull(groups = { NewGroup.class, EditGroup.class })
    @ExistingId(entity = Role.class, groups = { NewGroup.class, EditGroup.class })
    private ChildEntityProfile role;

    @Email(groups = { NewGroup.class, EditGroup.class })
    private String email;

    @Min(value = 100000000, groups = { NewGroup.class, EditGroup.class })
    @Max(value = 999999999, groups = { NewGroup.class, EditGroup.class })
    private Long phone;

    @Valid
    @ExistingId(entity = Parish.class, groups = { NewGroup.class, EditGroup.class })
    private ChildEntityProfile parish;

    @NotNull(groups = { NewGroup.class, EditGroup.class })
    private Boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChildEntityProfile getRole() {
        return role;
    }

    public void setRole(ChildEntityProfile role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public ChildEntityProfile getParish() {
        return parish;
    }

    public void setParish(ChildEntityProfile parish) {
        this.parish = parish;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public static class ChildEntityProfile {

        @NotNull(groups = { NewGroup.class, EditGroup.class })
        private Long id;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

    }

}
