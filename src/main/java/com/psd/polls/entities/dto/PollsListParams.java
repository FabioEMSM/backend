package com.psd.polls.entities.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class PollsListParams extends ListParams {

    @NotNull
    private String name = "";

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    public Date dateStart;

    @DateTimeFormat(pattern = "yyyy/MM/dd")
    public Date dateEnd;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

}
