package com.psd.polls.entities.dto.polldata;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;

public class TableData {

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Min(value = 1, groups = {NewGroup.class, EditGroup.class})
    private Long tableNumber;

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Min(value = 1, groups = {NewGroup.class, EditGroup.class})
    private Long registeredVoters;

    @Valid
    private List<UserData> users;

    public Long getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Long tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Long getRegisteredVoters() {
        return registeredVoters;
    }

    public void setRegisteredVoters(Long registeredVoters) {
        this.registeredVoters = registeredVoters;
    }

    public List<UserData> getUsers() {
        return users;
    }

    public void setUsers(List<UserData> users) {
        this.users = users;
    }

}
