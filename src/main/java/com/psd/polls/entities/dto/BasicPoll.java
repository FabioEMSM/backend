package com.psd.polls.entities.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.psd.polls.entities.dao.Poll;

public class BasicPoll extends BasicEntity {

    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date date;

    private Boolean isEditable;

    private Boolean isClosed;

    public BasicPoll(Long id, String name, Date date) {
        super(id, name);
        this.date = date;
    }

    public BasicPoll(Poll poll) {
        super(poll.getId(), poll.getName());
        this.date = poll.getDate();
        this.isEditable = poll.getIsEditable();
        this.isClosed = poll.getIsClosed();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

}
