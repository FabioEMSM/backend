package com.psd.polls.entities.dto.polldata;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;

public class PartyData extends ShortAndNameData {

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Pattern(regexp = "#([0-9a-fA-F]){6,6}", groups = {NewGroup.class, EditGroup.class})
    private String color;

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    private Boolean primary = false;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

}
