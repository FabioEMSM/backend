package com.psd.polls.entities.dto;

import javax.validation.constraints.NotNull;

public class UsersListParams extends ListParams {

    @NotNull
    private String name = "";

    @NotNull
    public String parish = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParish() {
        return parish;
    }

    public void setParish(String parish) {
        this.parish = parish;
    }

}
