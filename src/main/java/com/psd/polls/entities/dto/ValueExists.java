package com.psd.polls.entities.dto;

public class ValueExists {

    private Boolean exists = false;

    public ValueExists(Boolean exists) {
        this.exists = exists;
    }

    public Boolean getExists() {
        return exists;
    }

    public void setExists(Boolean exists) {
        this.exists = exists;
    }

}
