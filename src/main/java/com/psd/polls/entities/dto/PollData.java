package com.psd.polls.entities.dto;

import java.util.List;

import javax.validation.Valid;

import com.psd.polls.entities.dto.polldata.PollGeneralData;
import com.psd.polls.entities.dto.polldata.PollParishAssembliesData;
import com.psd.polls.entities.dto.polldata.PollPartiesData;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.validators.UniqueShortAndName;

public class PollData {

    @Valid
    private PollGeneralData general;

    @Valid
    private PollPartiesData cityCouncil;

    @Valid
    private PollPartiesData cityHall;

    @Valid
    @UniqueShortAndName(groups = {NewGroup.class, EditGroup.class})
    private List<PollParishAssembliesData> parishAssemblies;

    public PollGeneralData getGeneral() {
        return general;
    }

    public void setGeneral(PollGeneralData general) {
        this.general = general;
    }

    public PollPartiesData getCityCouncil() {
        return cityCouncil;
    }

    public void setCityCouncil(PollPartiesData cityCouncil) {
        this.cityCouncil = cityCouncil;
    }

    public PollPartiesData getCityHall() {
        return cityHall;
    }

    public void setCityHall(PollPartiesData cityHall) {
        this.cityHall = cityHall;
    }

    public List<PollParishAssembliesData> getParishAssemblies() {
        return parishAssemblies;
    }

    public void setParishAssemblies(List<PollParishAssembliesData> parishAssemblies) {
        this.parishAssemblies = parishAssemblies;
    }

}
