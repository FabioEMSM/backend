package com.psd.polls.entities.dto;

import org.springframework.data.domain.Page;

public class OutputList<T> {

    private Iterable<T> list;
    private Long total = 0L;
    private Long page = 0L;

    public OutputList(Page<T> page) {
        this.list = page.getContent();
        this.total = page.getTotalElements();
    }

    public Iterable<T> getList() {
        return list;
    }

    public void setList(Iterable<T> list) {
        this.list = list;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }
    
}
