package com.psd.polls.entities.dto;

import javax.validation.constraints.NotBlank;

public class UserLogin {

    @NotBlank
    public String username;

    @NotBlank
    public String password;
}
