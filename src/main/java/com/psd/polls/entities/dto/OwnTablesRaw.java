package com.psd.polls.entities.dto;

public class OwnTablesRaw {

    private Long pollId;

    private String pollName;

    private Long parishId;

    private String parishName;

    private Long tableNumber;

    public OwnTablesRaw (Long pollId, String pollName, Long parishId, String parishName, Long tableNumber) {
        this.pollId = pollId;
        this.pollName = pollName;
        this.parishId = parishId;
        this.parishName = parishName;
        this.tableNumber = tableNumber;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public String getPollName() {
        return pollName;
    }

    public void setPollName(String pollName) {
        this.pollName = pollName;
    }

    public Long getParishId() {
        return parishId;
    }

    public void setParishId(Long parishId) {
        this.parishId = parishId;
    }

    public String getParishName() {
        return parishName;
    }

    public void setParishName(String parishName) {
        this.parishName = parishName;
    }

    public Long getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Long tableNumber) {
        this.tableNumber = tableNumber;
    }

}
