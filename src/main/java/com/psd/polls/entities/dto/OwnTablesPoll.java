package com.psd.polls.entities.dto;

import java.util.List;

public class OwnTablesPoll extends BasicEntity {

    public OwnTablesPoll(Long id, String name) {
        super(id, name);
    }

    private List<OwnTablesParish> parishes;

    public List<OwnTablesParish> getParishes() {
        return parishes;
    }

    public void setParishes(List<OwnTablesParish> parishes) {
        this.parishes = parishes;
    }

}
