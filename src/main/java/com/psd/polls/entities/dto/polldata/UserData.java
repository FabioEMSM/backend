package com.psd.polls.entities.dto.polldata;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.validators.ExistingId;

public class UserData {

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Min(value = 1, groups = {NewGroup.class, EditGroup.class})
    @ExistingId(entity = User.class, groups = {NewGroup.class, EditGroup.class})
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
