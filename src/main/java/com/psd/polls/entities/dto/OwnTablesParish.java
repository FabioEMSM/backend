package com.psd.polls.entities.dto;

import java.util.List;

public class OwnTablesParish extends BasicEntity {

    public OwnTablesParish(Long id, String name) {
        super(id, name);
    }

    private List<Long> tables;

    public List<Long> getTables() {
        return tables;
    }

    public void setTables(List<Long> tables) {
        this.tables = tables;
    }

}
