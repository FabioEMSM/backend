package com.psd.polls.entities.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;

public class ListParams {

    private Integer page = 0;

    @NotNull
    private Integer itemsPerPage;

    @NotEmpty
    private String sortBy;

    @NotEmpty
    @Pattern(regexp = "^(asc|desc)$", flags = {Flag.CASE_INSENSITIVE})
    private String sortDir;

    public Integer getPage() {
        return (page != null) ? page : 0;
    }

    public void setPage(Integer page) {
        this.page = (page != null) ? page : 0;
    }

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

}
