package com.psd.polls.entities.dto.polldata;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;

import org.hibernate.validator.constraints.Length;

public class ShortAndNameData {

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Length(min = 2, max = 255, groups = {NewGroup.class, EditGroup.class})
    private String name;

    @JsonProperty("short")
    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @Length(min = 1, max = 32, groups = {NewGroup.class, EditGroup.class})
    private String shortName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

}
