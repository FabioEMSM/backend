package com.psd.polls.entities.dto.polldata;

import java.sql.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.validators.UniquePollName;

import org.hibernate.validator.constraints.Length;

@UniquePollName(groups = {NewGroup.class, EditGroup.class})
public class PollGeneralData {

    @NotNull(groups = { EditGroup.class })
    private Long id;

    @NotBlank(groups = {NewGroup.class, EditGroup.class})
    @Length(min = 5, max = 255, groups = {NewGroup.class, EditGroup.class})
    private String name;

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date date;

    @NotBlank(groups = {NewGroup.class, EditGroup.class})
    @Length(min = 5, max = 5, groups = {NewGroup.class, EditGroup.class})
    @Pattern(regexp = "([0-1][0-9]|2[0-3]):[0-5][0-9]", groups = {NewGroup.class, EditGroup.class})
    private String timeEnd;

    @NotBlank(groups = {NewGroup.class, EditGroup.class})
    @Length(min = 5, max = 5, groups = {NewGroup.class, EditGroup.class})
    @Pattern(regexp = "([0-1][0-9]|2[0-3]):[0-5][0-9]", groups = {NewGroup.class, EditGroup.class})
    private String timeStart;

    private Boolean isEditable = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

}
