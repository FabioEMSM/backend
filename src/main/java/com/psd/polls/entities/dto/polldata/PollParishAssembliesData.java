package com.psd.polls.entities.dto.polldata;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.psd.polls.entities.groups.EditGroup;
import com.psd.polls.entities.groups.NewGroup;
import com.psd.polls.validators.SinglePrimaryParty;
import com.psd.polls.validators.UniqueShortAndName;

public class PollParishAssembliesData extends ShortAndNameData {

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    private List<TableData> tables;

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    private Integer mandates;

    @NotNull(groups = {NewGroup.class, EditGroup.class})
    @UniqueShortAndName(groups = {NewGroup.class, EditGroup.class})
    @SinglePrimaryParty(groups = {NewGroup.class, EditGroup.class})
    private List<PartyData> parties;

    public List<TableData> getTables() {
        return tables;
    }

    public void setTables(List<TableData> tables) {
        this.tables = tables;
    }

    public Integer getMandates() {
        return mandates;
    }

    public void setMandates(Integer mandates) {
        this.mandates = mandates;
    }

    public List<PartyData> getParties() {
        return parties;
    }

    public void setParties(List<PartyData> parties) {
        this.parties = parties;
    }

}
