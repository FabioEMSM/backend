package com.psd.polls.entities.dao;

public enum PollPartyTarget {
    CITY_HALL("cityHall"),
    CITY_COUNCIL("cityCouncil");

    private final String label;

    private PollPartyTarget(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
