package com.psd.polls.entities.dao;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "polls")
public class Poll implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 255, unique = true)
    private String name;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "time_start", nullable = false)
    private String timeStart;

    @Column(name = "time_end", nullable = false)
    private String timeEnd;

    @Column(name = "city_hall_mandates", nullable = false)
    private Integer cityHallMandates;

    @Column(name = "city_council_mandates", nullable = false)
    private Integer cityCouncilMandates;

    @Column(name = "is_editable", nullable = false)
    private Boolean isEditable = true;

    @Column(name = "is_closed", nullable = false)
    private Boolean isClosed = false;

    @OneToMany(
        mappedBy = "id.poll",
        targetEntity = PollParty.class,
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL
    )
    @Where(clause = "target = 'cityHall'")
    @OrderBy("order")
    private Set<PollParty> cityHallParties;

    @OneToMany(
        mappedBy = "id.poll",
        targetEntity = PollParty.class,
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL
    )
    @Where(clause = "target = 'cityCouncil'")
    @OrderBy("order")
    private Set<PollParty> cityCouncilParties;

    @OneToMany(
        mappedBy = "id.poll",
        targetEntity = PollParish.class,
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL
    )
    @OrderBy("order")
    private Set<PollParish> parishAssemblies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getCityHallMandates() {
        return cityHallMandates;
    }

    public void setCityHallMandates(Integer cityHallMandates) {
        this.cityHallMandates = cityHallMandates;
    }

    public Integer getCityCouncilMandates() {
        return cityCouncilMandates;
    }

    public void setCityCouncilMandates(Integer cityCouncilMandates) {
        this.cityCouncilMandates = cityCouncilMandates;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

    public Set<PollParty> getCityHallParties() {
        return cityHallParties;
    }

    public void setCityHallParties(Set<PollParty> cityHallParties) {
        this.cityHallParties = cityHallParties;
    }

    public Set<PollParty> getCityCouncilParties() {
        return cityCouncilParties;
    }

    public void setCityCouncilParties(Set<PollParty> cityCouncilParties) {
        this.cityCouncilParties = cityCouncilParties;
    }

    public Set<PollParish> getParishAssemblies() {
        return parishAssemblies;
    }

    public void setParishAssemblies(Set<PollParish> parishAssemblies) {
        this.parishAssemblies = parishAssemblies;
    }

}
