package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class PollParishPartyPK implements Serializable {

    @ManyToOne()
    @JoinColumn(name = "poll_id", referencedColumnName = "poll_id")
    @JoinColumn(name = "parish_id", referencedColumnName = "parish_id")
    @JsonIgnore()
    private PollParish pollParish;

    @ManyToOne()
    @JoinColumn(name = "party_id")
    private Party party;

    public PollParish getPollParish() {
        return pollParish;
    }

    public void setPollParish(PollParish pollParish) {
        this.pollParish = pollParish;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

}
