package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class PollPartyPK implements Serializable {

    @ManyToOne(optional = false)
    @JoinColumn(name = "poll_id")
    @JsonIgnore()
    private Poll poll;

    @ManyToOne(optional = false)
    @JoinColumn(name = "party_id")
    private Party party;

    @Column(name = "target")
    private String target;

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(PollPartyTarget target) {
        this.target = target.getLabel();
    }

}
