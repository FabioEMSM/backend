package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class PollParishTableUserPK implements Serializable {

    @ManyToOne()
    @JoinColumn(name = "poll_id", referencedColumnName = "poll_id")
    @JoinColumn(name = "parish_id", referencedColumnName = "parish_id")
    @JoinColumn(name = "table_number", referencedColumnName = "table_number")
    @JsonIgnore()
    private PollParishTable parishTable;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    public PollParishTable getParishTable() {
        return parishTable;
    }

    public void setParishTable(PollParishTable parishTable) {
        this.parishTable = parishTable;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
