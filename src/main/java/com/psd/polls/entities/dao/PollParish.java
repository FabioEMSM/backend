package com.psd.polls.entities.dao;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "polls_parishes")
public class PollParish implements Serializable {

    @EmbeddedId()
    private PollParishPK id;

    @Column(name = "mandates", insertable = true, nullable = false)
    private Integer mandates;

    @Column(name = "order_number", nullable = false)
    private Long order;

    @OneToMany(
        mappedBy = "id.pollParish",
        cascade = CascadeType.ALL
    )
    private Set<PollParishTable> tables;

    @OneToMany(
        mappedBy = "id.pollParish",
        cascade = CascadeType.ALL
    )
    @OrderBy("order")
    private Set<PollParishParty> parties;

    public PollParishPK getId() {
        return id;
    }

    public void setId(PollParishPK id) {
        this.id = id;
    }

    public Integer getMandates() {
        return mandates;
    }

    public void setMandates(Integer mandates) {
        this.mandates = mandates;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public Set<PollParishTable> getTables() {
        return tables;
    }

    public void setTables(Set<PollParishTable> tables) {
        this.tables = tables;
    }

    public Set<PollParishParty> getParties() {
        return parties;
    }

    public void setParties(Set<PollParishParty> parties) {
        this.parties = parties;
    }

}
