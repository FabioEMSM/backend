package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class PollParishPK implements Serializable {

    @ManyToOne()
    @JoinColumn(name = "poll_id")
    @JsonIgnore()
    private Poll poll;

    @ManyToOne()
    @JoinColumn(name = "parish_id")
    private Parish parish;

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public Parish getParish() {
        return parish;
    }

    public void setParish(Parish parish) {
        this.parish = parish;
    }

}
