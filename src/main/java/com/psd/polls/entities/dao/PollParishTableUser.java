package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "polls_parishes_tables_users")
public class PollParishTableUser implements Serializable {

    @EmbeddedId
    private PollParishTableUserPK id;

    public PollParishTableUserPK getId() {
        return id;
    }

    public void setId(PollParishTableUserPK id) {
        this.id = id;
    }

}
