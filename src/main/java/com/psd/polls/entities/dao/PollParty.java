package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "polls_parties")
public class PollParty implements Serializable {

    @EmbeddedId
    private PollPartyPK id;

    @Column(name = "is_primary", nullable = false)
    private Boolean primary;

    @Column(name = "order_number", nullable = false)
    private Long order;

    public PollPartyPK getId() {
        return id;
    }

    public void setId(PollPartyPK id) {
        this.id = id;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

}
