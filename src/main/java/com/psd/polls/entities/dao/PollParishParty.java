package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "polls_parishes_parties")
public class PollParishParty implements Serializable {

    @EmbeddedId
    private PollParishPartyPK id;

    @Column(name = "is_primary", nullable = false)
    private Boolean primary;

    @Column(name = "order_number", nullable = false)
    private Long order;

    public PollParishPartyPK getId() {
        return id;
    }

    public void setId(PollParishPartyPK id) {
        this.id = id;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

}
