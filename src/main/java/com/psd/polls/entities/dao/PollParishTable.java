package com.psd.polls.entities.dao;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "polls_parishes_tables")
public class PollParishTable implements Serializable {

    @EmbeddedId
    private PollParishTablePK id;

    @Column(name = "registered_voters")
    private Long registeredVoters;

    @OneToMany(
        mappedBy = "id.parishTable",
        cascade = CascadeType.ALL
    )
    private Set<PollParishTableUser> users;

    public PollParishTablePK getId() {
        return id;
    }

    public void setId(PollParishTablePK id) {
        this.id = id;
    }

    public Long getRegisteredVoters() {
        return registeredVoters;
    }

    public void setRegisteredVoters(Long registeredVoters) {
        this.registeredVoters = registeredVoters;
    }

    public Set<PollParishTableUser> getUsers() {
        return users;
    }

    public void setUsers(Set<PollParishTableUser> users) {
        this.users = users;
    }

}
