package com.psd.polls.entities.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class PollParishTablePK implements Serializable {

    @ManyToOne()
    @JoinColumn(name = "poll_id", referencedColumnName = "poll_id")
    @JoinColumn(name = "parish_id", referencedColumnName = "parish_id")
    @JsonIgnore()
    private PollParish pollParish;

    @Column(name = "table_number", nullable = false)
    private Long tableNumber;

    public PollParish getPollParish() {
        return pollParish;
    }

    public void setPollParish(PollParish pollParish) {
        this.pollParish = pollParish;
    }

    public Long getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Long tableNumber) {
        this.tableNumber = tableNumber;
    }

}
