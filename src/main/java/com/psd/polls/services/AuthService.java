package com.psd.polls.services;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import com.psd.polls.configurations.helpers.GenerateCookieString;
import com.psd.polls.configurations.helpers.UserInSession;
import com.psd.polls.entities.dao.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    private UsersService usersService;

    public void doLogout(HttpServletResponse response) {
        User user = UserInSession.get();
        if (user == null) {
            response.setStatus(500);
            return;
        }

        user.setRemember(null);
        usersService.getUsersRepository().save(user);

        UserInSession.destroy();
        destroyCookie(response);

        response.setStatus(200);
    }

    private void destroyCookie(HttpServletResponse response) {
        Date currentDateTime = new Date(System.currentTimeMillis());
        response.setHeader(
            "Set-cookie",
            GenerateCookieString.getCookieHeaderString(
                "Auth-refresh",
                "",
                1,
                currentDateTime
            )
        );
    }
}
