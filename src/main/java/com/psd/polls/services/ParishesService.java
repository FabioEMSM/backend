package com.psd.polls.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.psd.polls.entities.dao.Parish;
import com.psd.polls.entities.dto.polldata.PollParishAssembliesData;
import com.psd.polls.repositories.ParishesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class ParishesService {

    @Autowired
    private ParishesRepository parishesRepository;

    public List<Parish> getParishes(String name) {
        return parishesRepository.findAllParishesBySearchString(name);
    }

    public List<Parish> getParishesFromUsers(String name) {
        return parishesRepository
            .findParishesBySearchString(
                name,
                PageRequest.of(
                    0,
                    5,
                    Sort.by(
                        Direction.ASC,
                        "name"
                    )
                )
            )
            .stream()
            .map((Parish parish) -> parish)
            .collect(Collectors.toList());
    }

    public Parish getParishOrCreate(PollParishAssembliesData parishData) {
        Optional<Parish> optionalParish = parishesRepository
            .findByNameIgnoreCaseAndShortNameIgnoreCase(parishData.getName(), parishData.getShortName());

        if (optionalParish.isPresent()) {
            return optionalParish.get();
        }

        Parish parish = new Parish();
        parish.setName(parishData.getName());
        parish.setShortName(parishData.getShortName());

        return parishesRepository.save(parish);
    }

}
