package com.psd.polls.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.psd.polls.configurations.PasswordEncription;
import com.psd.polls.configurations.helpers.UserInSession;
import com.psd.polls.entities.dao.Parish;
import com.psd.polls.entities.dao.Role;
import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.dto.BasicEntity;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.entities.dto.UserProfile;
import com.psd.polls.entities.dto.UsersListParams;
import com.psd.polls.repositories.ParishesRepository;
import com.psd.polls.repositories.RolesRepository;
import com.psd.polls.repositories.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UsersService implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ParishesRepository parishesRepository;

    @Autowired
    private RolesRepository rolesRepository;

    @Autowired
    private PasswordEncription passwordEncription;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = usersRepository.findByUsernameIgnoreCase(username);

        if (user == null) {
            throw new UsernameNotFoundException("A User with username '" + username + "' was not found");
        }

        return user;
    }

    public Boolean usernameExists(String username) {
        return usersRepository.existsByUsernameIgnoreCase(username);
    }

    public User createUser(UserProfile newUserProfile) {
        User newUser = new User();
        newUser.setUsername(newUserProfile.getUsername());
        newUser.setPassword(passwordEncription.encoder().encode(newUserProfile.getPassword()));

        newUser.setName(newUserProfile.getName());

        Optional<Role> optionalRole = rolesRepository.findById(newUserProfile.getRole().getId());
        if (!optionalRole.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        newUser.setRole(optionalRole.get());

        newUser.setPhone(newUserProfile.getPhone());
        newUser.setEmail(newUserProfile.getEmail());

        Parish parish = null;
        if (newUserProfile.getParish() != null) {
            Optional<Parish> optionalParish = parishesRepository.findById(newUserProfile.getParish().getId());

            if (optionalParish.isPresent()) {
                parish = optionalParish.get();
            }
        }
        newUser.setParish(parish);

        newUser.setEnabled(newUserProfile.getEnabled());

        return usersRepository.save(newUser);
    }

    public User editUser(User user, UserProfile newUserProfile) {
        if (!user.getId().equals(newUserProfile.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        User selfUser = UserInSession.get();
        if (!selfUser.getId().equals(user.getId())) {
            Optional<Role> optionalRole = rolesRepository.findById(newUserProfile.getRole().getId());
            if (!optionalRole.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            user.setRole(optionalRole.get());

            user.setEnabled(newUserProfile.getEnabled());
        }

        if (newUserProfile.getPassword() != null) {
            user.setPassword(passwordEncription.encoder().encode(newUserProfile.getPassword()));
        }

        user.setName(newUserProfile.getName());
        user.setPhone(newUserProfile.getPhone());
        user.setEmail(newUserProfile.getEmail());

        Parish parish = null;
        if (newUserProfile.getParish() != null) {
            Optional<Parish> optionalParish = parishesRepository.findById(newUserProfile.getParish().getId());

            if (optionalParish.isPresent()) {
                parish = optionalParish.get();
            }
        }
        user.setParish(parish);

        return usersRepository.save(user);
    }

    public OutputList<User> getUsersList(final UsersListParams usersListParams) {
        return new OutputList<>(
            usersRepository.findUsersBySearchNameAndParish(
                usersListParams.getName(),
                usersListParams.getParish(),
                PageRequest.of(
                    usersListParams.getPage(),
                    usersListParams.getItemsPerPage(),
                    Sort.by(
                        usersListParams.getSortDir().equalsIgnoreCase("ASC") ?
                            Direction.ASC :
                            Direction.DESC,
                        usersListParams.getSortBy().equalsIgnoreCase("parish")  ?
                            "parish.name" :
                            usersListParams.getSortBy()
                    )
                )
            )
        );
    }

    public List<BasicEntity> getUsersFilter(final String name) {
        return usersRepository
            .findUsersBySearchString(
                name,
                PageRequest.of(
                    0,
                    5,
                    Sort.by(
                        Direction.ASC,
                        "name"
                    )
                )
            )
            .stream()
            .map((User user) -> new BasicEntity(user.getId(), user.getName()))
            .collect(Collectors.toList());
    }

    public User findUserByAccessToken(String accessToken) {
        return usersRepository.findByRemember(accessToken);
    }

    public UsersRepository getUsersRepository() {
        return usersRepository;
    }

    public void toggleUser(User user) {
        validateNotSelfUser(user);

        user.setEnabled(!user.isEnabled());
        usersRepository.save(user);
    }

    public void deleteUser(User user) {
        validateNotSelfUser(user);
        usersRepository.delete(user);
    }

    private void validateNotSelfUser(User user) {
        if(UserInSession.get().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

}