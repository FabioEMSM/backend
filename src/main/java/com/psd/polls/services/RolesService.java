package com.psd.polls.services;

import com.psd.polls.entities.dao.Role;
import com.psd.polls.repositories.RolesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolesService {

    @Autowired
    private RolesRepository rolesRepository;

    public Iterable<Role> getRoles() {
        return rolesRepository.findAll();
    }

}
