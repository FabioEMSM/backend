package com.psd.polls.services;

import java.util.Optional;

import com.psd.polls.entities.dao.Party;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.entities.dto.polldata.PartyData;
import com.psd.polls.repositories.PartiesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class PartiesService {

    @Autowired
    private PartiesRepository partiesRepository;

    public OutputList<Party> getPartiesList(final String name) {
        return new OutputList<>(
            partiesRepository.findPartiesBySearchName(
                name,
                PageRequest.of(
                    0,
                    5,
                    Sort.by(
                        Direction.ASC,
                        "name"
                    )
                )
            )
        );
    }

    public Party getPartyOrCreate(PartyData partyData) {
        Optional<Party> optionalParty = partiesRepository
            .findByNameIgnoreCaseAndShortNameIgnoreCase(partyData.getName(), partyData.getShortName());

        if (optionalParty.isPresent()) {
            return optionalParty.get();
        }

        Party party = new Party();
        party.setName(partyData.getName());
        party.setShortName(partyData.getShortName());
        party.setColor(partyData.getColor());

        return partiesRepository.save(party);
    }
}
