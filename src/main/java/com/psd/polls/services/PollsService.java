package com.psd.polls.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.psd.polls.configurations.helpers.UserInSession;
import com.psd.polls.entities.dao.Parish;
import com.psd.polls.entities.dao.Party;
import com.psd.polls.entities.dao.Poll;
import com.psd.polls.entities.dao.PollParish;
import com.psd.polls.entities.dao.PollParishPK;
import com.psd.polls.entities.dao.PollParishParty;
import com.psd.polls.entities.dao.PollParishPartyPK;
import com.psd.polls.entities.dao.PollParishTable;
import com.psd.polls.entities.dao.PollParishTablePK;
import com.psd.polls.entities.dao.PollParishTableUser;
import com.psd.polls.entities.dao.PollParishTableUserPK;
import com.psd.polls.entities.dao.PollParty;
import com.psd.polls.entities.dao.PollPartyPK;
import com.psd.polls.entities.dao.PollPartyTarget;
import com.psd.polls.entities.dao.User;
import com.psd.polls.entities.dto.BasicEntity;
import com.psd.polls.entities.dto.BasicPoll;
import com.psd.polls.entities.dto.OutputList;
import com.psd.polls.entities.dto.OwnTablesParish;
import com.psd.polls.entities.dto.OwnTablesPoll;
import com.psd.polls.entities.dto.OwnTablesRaw;
import com.psd.polls.entities.dto.PollData;
import com.psd.polls.entities.dto.PollsListParams;
import com.psd.polls.entities.dto.polldata.PartyData;
import com.psd.polls.entities.dto.polldata.PollGeneralData;
import com.psd.polls.entities.dto.polldata.PollParishAssembliesData;
import com.psd.polls.entities.dto.polldata.PollPartiesData;
import com.psd.polls.entities.dto.polldata.TableData;
import com.psd.polls.entities.dto.polldata.UserData;
import com.psd.polls.repositories.PollParishTablesRepository;
import com.psd.polls.repositories.PollParishesRepository;
import com.psd.polls.repositories.PollPartiesRepository;
import com.psd.polls.repositories.PollsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class PollsService {

    @Autowired
    private PollsRepository pollsRepository;

    @Autowired
    private PollPartiesRepository pollPartiesRepository;

    @Autowired
    private PollParishesRepository pollParishesRepository;

    @Autowired
    private PollParishTablesRepository pollParishTablesRepository;

    @Autowired
    private PartiesService partiesService;

    @Autowired
    private ParishesService parishesService;

    @Autowired
    private UsersService usersService;

    public OutputList<BasicPoll> getPollsList(final PollsListParams pollsListParams) {
        return new OutputList<>(
            pollsRepository
                .findPollsBySearchNameAndDate(
                    pollsListParams.getName(),
                    pollsListParams.getDateStart(),
                    pollsListParams.getDateEnd(),
                    PageRequest.of(
                        pollsListParams.getPage(),
                        pollsListParams.getItemsPerPage(),
                        Sort.by(
                            pollsListParams.getSortDir().equalsIgnoreCase("ASC") ?
                                Direction.ASC :
                                Direction.DESC,
                            pollsListParams.getSortBy()
                        )
                    )
                )
                .map(BasicPoll::new)
        );
    }

    public List<BasicEntity> getPollsFilter(final String name) {
        return pollsRepository
            .findPollsBySearchString(
                name,
                PageRequest.of(
                    0,
                    5,
                    Sort.by(
                        Direction.ASC,
                        "name"
                    )
                )
            )
            .stream()
            .map((Poll poll) -> new BasicEntity(poll.getId(), poll.getName()))
            .collect(Collectors.toList());
    }

    public Boolean pollNameExists(String poll, Long pollId) {
        return pollsRepository.existsByNameIgnoreCaseAndIdNot(poll, (pollId == null) ? 0 : pollId);
    }

    public PollData getPollData(final Poll poll) {
        PollData pollData = new PollData();

        PollGeneralData pollGeneralData = new PollGeneralData();
        pollGeneralData.setId(poll.getId());
        pollGeneralData.setName(poll.getName());
        pollGeneralData.setDate(poll.getDate());
        pollGeneralData.setTimeStart(poll.getTimeStart().substring(0, 5));
        pollGeneralData.setTimeEnd(poll.getTimeEnd().substring(0, 5));
        pollGeneralData.setIsEditable(poll.getIsEditable());
        pollData.setGeneral(pollGeneralData);

        PollPartiesData cityHallParties = new PollPartiesData();
        cityHallParties.setMandates(poll.getCityHallMandates());
        cityHallParties.setParties(createPollPartiesFromList(poll.getCityHallParties()));
        pollData.setCityHall(cityHallParties);

        PollPartiesData cityCouncilParties = new PollPartiesData();
        cityCouncilParties.setMandates(poll.getCityCouncilMandates());
        cityCouncilParties.setParties(createPollPartiesFromList(poll.getCityCouncilParties()));
        pollData.setCityCouncil(cityCouncilParties);

        pollData.setParishAssemblies(createPollParishesFromList(poll.getParishAssemblies()));

        return pollData;
    }

    private List<PollParishAssembliesData> createPollParishesFromList(Set<PollParish> pollParishList) {
        List<PollParishAssembliesData> parishDataList = new ArrayList<>();

        for (PollParish pollParish : pollParishList) {
            PollParishAssembliesData parishData = new PollParishAssembliesData();
            parishData.setName(pollParish.getId().getParish().getName());
            parishData.setShortName(pollParish.getId().getParish().getShortName());
            parishData.setMandates(pollParish.getMandates());
            parishData.setTables(createPollParishTablesFromList(pollParish.getTables()));
            parishData.setParties(createPollParishPartiesFromList(pollParish.getParties()));
            parishDataList.add(parishData);
        }

        return parishDataList;
    }

    private List<TableData> createPollParishTablesFromList(Set<PollParishTable> pollParishTableList) {
        List<TableData> parishTableDataList = new ArrayList<>();

        for (PollParishTable pollParishTable : pollParishTableList) {
            TableData parishTableData = new TableData();
            parishTableData.setTableNumber(pollParishTable.getId().getTableNumber());
            parishTableData.setRegisteredVoters(pollParishTable.getRegisteredVoters());
            parishTableData.setUsers(createPollParishTableUsersFromList(pollParishTable.getUsers()));
            parishTableDataList.add(parishTableData);
        }

        return parishTableDataList;
    }

    private List<UserData> createPollParishTableUsersFromList(Set<PollParishTableUser> pollParishTableUserList) {
        List<UserData> parishTableUserDataList = new ArrayList<>();

        for (PollParishTableUser pollParishTableUser : pollParishTableUserList) {
            UserData parishTableUserData = new UserData();
            parishTableUserData.setId(pollParishTableUser.getId().getUser().getId());
            parishTableUserData.setName(pollParishTableUser.getId().getUser().getName());
            parishTableUserDataList.add(parishTableUserData);
        }

        return parishTableUserDataList;
    }

    private List<PartyData> createPollParishPartiesFromList(Set<PollParishParty> pollParishPartyList) {
        List<PartyData> parishPartyDataList = new ArrayList<>();

        for (PollParishParty pollParishParty : pollParishPartyList) {
            PartyData parishPartyData = new PartyData();
            parishPartyData.setName(pollParishParty.getId().getParty().getName());
            parishPartyData.setShortName(pollParishParty.getId().getParty().getShortName());
            parishPartyData.setColor(pollParishParty.getId().getParty().getColor());
            parishPartyData.setPrimary(pollParishParty.getPrimary());
            parishPartyDataList.add(parishPartyData);
        }

        return parishPartyDataList;
    }

    private List<PartyData> createPollPartiesFromList(Set<PollParty> pollPartyList) {
        List<PartyData> partyDataList = new ArrayList<>();

        for (PollParty pollParty : pollPartyList) {
            PartyData partyData = new PartyData();
            partyData.setColor(pollParty.getId().getParty().getColor());
            partyData.setName(pollParty.getId().getParty().getName());
            partyData.setShortName(pollParty.getId().getParty().getShortName());
            partyData.setPrimary(pollParty.getPrimary());
            partyDataList.add(partyData);
        }

        return partyDataList;
    }

    @Transactional
    public Poll createPoll(PollData pollData) {
        Poll poll = new Poll();
        return setPollDataAndPersist(pollData, poll);
    }

    @Transactional
    public Poll editPoll(PollData pollData, Poll poll) {
        if (!poll.getId().equals(pollData.getGeneral().getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        pollPartiesRepository.deleteAll(poll.getCityCouncilParties());
        pollPartiesRepository.deleteAll(poll.getCityHallParties());
        pollParishesRepository.deleteAll(poll.getParishAssemblies());
        poll.setCityCouncilParties(null);
        poll.setCityHallParties(null);
        poll.setParishAssemblies(null);

        return setPollDataAndPersist(pollData, pollsRepository.saveAndFlush(poll));
    }

    public Poll setPollDataAndPersist(PollData pollData, Poll poll) {
        poll.setDate(new Date(pollData.getGeneral().getDate().getTime()));
        poll.setName(pollData.getGeneral().getName());
        poll.setTimeStart(pollData.getGeneral().getTimeStart());
        poll.setTimeEnd(pollData.getGeneral().getTimeEnd());
        poll.setCityHallMandates(pollData.getCityHall().getMandates());
        poll.setCityCouncilMandates(pollData.getCityCouncil().getMandates());

        poll.setCityHallParties(
            setPollPartiesList(
                pollData.getCityHall().getParties(),
                poll,
                PollPartyTarget.CITY_HALL
            )
        );

        poll.setCityCouncilParties(
            setPollPartiesList(
                pollData.getCityCouncil().getParties(),
                poll,
                PollPartyTarget.CITY_COUNCIL
            )
        );

        poll.setParishAssemblies(setPollParishesList(pollData.getParishAssemblies(), poll));

        return pollsRepository.save(poll);
    }

    public void deletePoll(Poll poll) {
        // TODO: Remove notice images

        pollsRepository.delete(poll);
    }

    public void lockPollEdition(Poll poll) {
        poll.setIsEditable(false);
        pollsRepository.save(poll);
    }

    public void finalizePoll(Poll poll) {
        if (!Boolean.FALSE.equals(poll.getIsEditable())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // TODO: Validate all pending notices

        poll.setIsClosed(true);
        pollsRepository.save(poll);
    }

    private Set<PollParish> setPollParishesList(
        final List<PollParishAssembliesData> rawParishesList,
        final Poll poll
    ) {
        Long order = 0L;
        Set<PollParish> pollParishesList = new HashSet<>();

        for (PollParishAssembliesData parishData : rawParishesList) {
            Parish parish = parishesService.getParishOrCreate(parishData);
            PollParishPK pollParishPK = new PollParishPK();
            pollParishPK.setPoll(poll);
            pollParishPK.setParish(parish);
            PollParish pollParish = new PollParish();
            pollParish.setId(pollParishPK);
            pollParish.setMandates(parishData.getMandates());
            pollParish.setOrder(++order);

            pollParish.setTables(setPollParishTablesList(parishData.getTables(), pollParish));
            pollParish.setParties(setPollParishPartiesList(parishData.getParties(), pollParish));

            pollParishesList.add(pollParish);
        }

        return pollParishesList;
    }

    private Set<PollParishTable> setPollParishTablesList(
        final List<TableData> rawParishTablesList,
        final PollParish pollParish
    ) {
        Set<PollParishTable> pollParishTablesList = new HashSet<>();

        for (TableData parishTableData : rawParishTablesList) {
            PollParishTablePK pollParishTablePK = new PollParishTablePK();
            pollParishTablePK.setPollParish(pollParish);
            pollParishTablePK.setTableNumber(parishTableData.getTableNumber());
            PollParishTable pollParishTable = new PollParishTable();
            pollParishTable.setId(pollParishTablePK);
            pollParishTable.setRegisteredVoters(parishTableData.getRegisteredVoters());

            pollParishTable.setUsers(
                setPollParishTableUsersList(
                    parishTableData.getUsers(),
                    pollParishTable
                )
            );

            pollParishTablesList.add(pollParishTable);
        }

        return pollParishTablesList;
    }

    private Set<PollParishTableUser> setPollParishTableUsersList(
        final List<UserData> rawParishTableUsersList,
        final PollParishTable pollParishTable
    ) {
        Set<PollParishTableUser> parishTableUserList = new HashSet<>();

        for (UserData parishTableUserData : rawParishTableUsersList) {
            Optional<User> optionalUser = usersService.getUsersRepository().findById(parishTableUserData.getId());
            if (!optionalUser.isPresent()) {
                continue;
            }

            PollParishTableUserPK pollParishTableUserPK = new PollParishTableUserPK();
            pollParishTableUserPK.setParishTable(pollParishTable);
            pollParishTableUserPK.setUser(optionalUser.get());
            PollParishTableUser pollParishTableUser = new PollParishTableUser();
            pollParishTableUser.setId(pollParishTableUserPK);

            parishTableUserList.add(pollParishTableUser);
        }

        return parishTableUserList;
    }

    private Set<PollParishParty> setPollParishPartiesList(
        final List<PartyData> rawPartiesList,
        final PollParish pollParish
    ) {
        Long order = 0L;
        Set<PollParishParty> parishPartiesList = new HashSet<>();

        for (PartyData partyData : rawPartiesList) {
            Party party = partiesService.getPartyOrCreate(partyData);
            PollParishPartyPK pollParishPartyPK = new PollParishPartyPK();
            pollParishPartyPK.setPollParish(pollParish);
            pollParishPartyPK.setParty(party);
            PollParishParty pollParishParty = new PollParishParty();
            pollParishParty.setId(pollParishPartyPK);
            pollParishParty.setPrimary(partyData.getPrimary());
            pollParishParty.setOrder(++order);

            parishPartiesList.add(pollParishParty);
        }

        return parishPartiesList;
    }

    private Set<PollParty> setPollPartiesList(
        final List<PartyData> rawPartiesList,
        final Poll poll,
        final PollPartyTarget target
    ) {
        Long order = 0L;
        Set<PollParty> partiesList = new HashSet<>();

        for (PartyData partyData : rawPartiesList) {
            Party party = partiesService.getPartyOrCreate(partyData);
            PollPartyPK pollPartyPK = new PollPartyPK();
            pollPartyPK.setPoll(poll);
            pollPartyPK.setParty(party);
            pollPartyPK.setTarget(target);
            PollParty pollParty = new PollParty();
            pollParty.setId(pollPartyPK);
            pollParty.setOrder(++order);
            pollParty.setPrimary(partyData.getPrimary());

            partiesList.add(pollParty);
        }

        return partiesList;
    }

    public List<OwnTablesPoll> getOwnTables() {
        User currentUser = UserInSession.get();

        return prepareOwnTablesCollection(
            currentUser.getRole().getName().equalsIgnoreCase("Gestor") ?
                getAllActiveTables() :
                usersTables(currentUser)
        );
    }

    private List<OwnTablesPoll> prepareOwnTablesCollection(List<OwnTablesRaw> ownTablesList) {
        List<OwnTablesPoll> collection = new ArrayList<>();

        for (OwnTablesRaw ownTablesRaw : ownTablesList) {
            Optional<OwnTablesPoll> optionalPoll =
                (Optional<OwnTablesPoll>)findInCollection(collection, ownTablesRaw.getPollId());

            OwnTablesPoll poll;

            if(optionalPoll.isPresent()) {
                poll = optionalPoll.get();
            }
            else {
                poll = new OwnTablesPoll(ownTablesRaw.getPollId(), ownTablesRaw.getPollName());
                poll.setParishes(new ArrayList<>());
                collection.add(poll);
            }

            List<OwnTablesParish> parishesCollection = poll.getParishes();
            Optional<OwnTablesParish> optionalParish =
                (Optional<OwnTablesParish>)findInCollection(parishesCollection, ownTablesRaw.getParishId());

            OwnTablesParish parish;

            if(optionalParish.isPresent()) {
                parish = optionalParish.get();
            }
            else {
                parish = new OwnTablesParish(ownTablesRaw.getParishId(), ownTablesRaw.getParishName());
                parish.setTables(new ArrayList<>());
                parishesCollection.add(parish);
            }

            List<Long> tableNumbersCollection = parish.getTables();
            Optional<Long> optionalTableNumber = tableNumbersCollection.stream()
                .filter(item -> item.equals(ownTablesRaw.getTableNumber()))
                .findFirst();

            if (!optionalTableNumber.isPresent()) {
                tableNumbersCollection.add(ownTablesRaw.getTableNumber());
            }
        }

        return collection;
    }

    private Optional<? extends BasicEntity> findInCollection(List<? extends BasicEntity> collection, Long needle) {
        return collection.stream()
                .filter(item -> item.getId().equals(needle))
                .findFirst();
    }

    private List<OwnTablesRaw> getAllActiveTables() {
        return pollParishTablesRepository.findAllActiveTables();
    }

    private List<OwnTablesRaw> usersTables(User user) {
        return pollParishTablesRepository.findAllActiveTablesByUser(user);
    }

}
